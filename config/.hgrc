[ui]
username = Elouan Martinet <exa@elou.world>
ignore = ~/.hgignore
ignore.global = ~/.hgignore
ignore.local = .hg/hgignore
interface = curses
origbackuppath = .hg/backup

[experimental]
evolution = all

[extensions]
color =
evolve =
hgk =
histedit =
pager =
patchbomb =
rebase =
schemes =
shelve =
topic =
absorb =

[phases]
publish = false
new-commit = secret

[hooks]
pre-rebase = hg-pre-rebase

[committemplate]
changeset = {desc}\n\n
	HG: {extramsg}
	HG: --
	HG: user: {author}\n{ifeq(p2rev, "-1", "",
	"HG: branch merge\n")
	}HG: branch '{branch}'
	HG: topic '{topic|nonempty}'
	HG: phase {phase}\n{if(activebookmark,
	"HG: bookmark '{activebookmark}'\n")   }{subrepos %
	"HG: subrepo {subrepo}\n"              }{file_adds %
	"HG: added {file}\n"                   }{file_mods %
	"HG: changed {file}\n"                 }{file_dels %
	"HG: removed {file}\n"                 }{if(files, "",
	"HG: no files changed\n")}HG: ------------------------ >8 ------------------------
	{diff()}

[color]
log.topic = cyan
log.branch = blue
changeset.public = green bold
changeset.draft = yellow bold
changeset.secret = red bold

[alias]
a = add
apply = import --no-commit
ex = export
exf = status --rev 'parents($1)' --rev $1 -m
exl = log -p -r
h = help
he = histedit
l = log -G
ls = files
mark = resolve --mark
reb = rebase
rebpub = rebase -s $1 -d 'last(public() and branch(.))'
rebstack = rebase -s 'first(ancestors(.) and not public() and branch(.))' -d 'last(public() and branch(.))'
restore = revert
rv = paths
upl = up 'last(public() and branch(.))'
wip = log --graph --rev=wip --template=wip

[pager]
attend-ex = true
attend-exl = true
attend-h = true
attend-help = true
attend-l = true
attend-wip = true

[templates]
wip = '{label("log.branch", branch)}{if(topic, "/{label("log.topic", topic)}")} {label("changeset.{phase}", "{rev}:{node|shortest}")} {label("grep.user", author|user)}{label("log.tag", if(tags," [{tags}]"))}{bookmarks % "{ifeq(bookmark, currentbookmark, label('log.activebookmark', bookmark), label('log.bookmark', " {bookmark}"))} "}\n{label("log.summary", fill(desc, "48")|firstline)} {date|age}'

[revsetalias]
wip = (parents(not public()) or not public() or . or head()) and (not obsolete() or unstable()^) and not closed()

[phabsend]
confirm = true

[patchbomb]
cc=
