# loaded by */set-env.* scripts
directory_names=('internal' 'env')
if [ -n "${interactive_shell}" ]; then
	directory_names=(${directory_names[@]} 'function' 'alias' 'completion')
fi
for directory_name in ${directory_names[@]}; do
	for file in "${DOTFILES}"/shell/${directory_name}/*; do
		if [ -f "${file}" ]; then
			case "${file}" in
				(*.bak) ;;
				(*) source "${file}" ;;
			esac
		fi
	done
done
unset directory_name
unset directory_names
