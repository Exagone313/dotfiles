#!/usr/bin/env python3
"""
Copyright (C) 2020 Elouan Martinet <exa@elou.world>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived
from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from re import sub


def entrypoint():
    args = get_program_args()
    fname = args.file

    results = {}
    ignored = []
    for vcard in extract_vcards(fname):
        if args.normalize_phone_numbers:
            normalize_phone_numbers(vcard, str(args.country_code))
        deduplicate_vcards(results, ignored, vcard, args.deduplication_key)

    full_results = ignored
    for key in results:
        full_results.append(results[key])

    for vcard in full_results:
        export_vcard(vcard)


def extract_vcards(fname):
    vcard_open = False
    with open(fname) as fobj:
        while True:
            line = fobj.readline()
            if not line:
                break
            line = line.strip()
            if not vcard_open:
                if line == 'BEGIN:VCARD':
                    vcard_open = True
                    raw_vcard = {}
            else:
                if line == 'END:VCARD':
                    yield transform_raw_vcard(raw_vcard)
                    vcard_open = False
                else:
                    key, value = line.split(':', 1)
                    if key not in raw_vcard:
                        raw_vcard[key] = value


def transform_raw_vcard(raw_vcard):
    vcard = {}
    for raw_key in raw_vcard:
        raw_key_parts = raw_key.split(';')
        key = raw_key_parts[0]
        parts = raw_key_parts[1:]

        values = {
            '_values': raw_vcard[raw_key].split(';')
        }

        for part in parts:
            try:
                part_key, part_value = part.split('=', 1)
            except ValueError:
                part_key, part_value = part, True
            values[part_key] = part_value

        if key in vcard:
            if not isinstance(vcard[key], list):
                vcard[key] = [vcard[key]]
            vcard[key].append(values)
        else:
            vcard[key] = values
    return vcard


def normalize_phone_numbers(vcard, country_code):
    if 'TEL' not in vcard:
        return

    if isinstance(vcard['TEL'], list):
        tels = vcard['TEL']
    else:
        tels = [vcard['TEL']]

    for tel in tels:
        if len(tel['_values']) != 1 or not tel['_values'][0]:
            continue
        tel['_values'][0] = normalize_phone_number(
            tel['_values'][0], country_code)


def normalize_phone_number(number, country_code):
    number = sub('[^0-9+]', '', number)
    if number[0] == '0':
        number = '+' + country_code + number[1:]
    return number


def deduplicate_vcards(results, ignored, vcard, deduplication_key='FN'):
    if deduplication_key not in vcard \
            or isinstance(vcard[deduplication_key], list) \
            or len(vcard[deduplication_key]['_values']) != 1:
        ignored.append(vcard)
        return
    value = vcard[deduplication_key]['_values'][0]
    # TODO choose between first or last
    results[value] = vcard


def export_vcard(vcard):
    print('BEGIN:VCARD')
    for key in vcard:
        key_values = vcard[key]
        if not isinstance(key_values, list):
            key_values = [key_values]
        for key_value in key_values:
            values = ';'.join(key_value['_values'])
            del key_value['_values']
            params = [key]
            for param_key in key_value:
                if key_value[param_key] is True:
                    params.append(param_key)
                else:
                    params.append(param_key + '=' + key_value[param_key])
            key_with_params = ';'.join(params)
            print(key_with_params + ':' + values)
    print('END:VCARD')


def get_program_args():
    parser = ArgumentParser(description='Deduplicate VCF cards',
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--country-code', default=33, type=int,
                        help='Country calling code')
    parser.add_argument('-n', '--normalize-phone-numbers', action='store_true',
                        help=('Normalize phone numbers using country '
                              'calling code'))
    parser.add_argument('-k', '--deduplication-key', default='FN',
                        help='Deduplication key')
    parser.add_argument('file', help='VCF file to deduplicate')
    return parser.parse_args()


if __name__ == "__main__":
    entrypoint()
