dotfiles_git_branch=''
dotfiles_git_dirty=''

dotfiles_git_vars() {
	dotfiles_git_branch="$(git symbolic-ref HEAD 2>/dev/null)" && \
		dotfiles_git_branch="${dotfiles_git_branch#'refs/heads/'}" || \
		dotfiles_git_branch="$(git rev-parse --short HEAD 2>/dev/null)" || \
		dotfiles_git_branch=''
	if [ -n "${dotfiles_git_branch}" ] && \
			[ -n "$(git status --porcelain --ignore-submodules=dirty 2>/dev/null | head -n1)" ]; then
		dotfiles_git_dirty='1'
		return
	fi
	dotfiles_git_dirty=''
}

dotfiles_hg_root=''
dotfiles_hg_branch=''
dotfiles_hg_topic=''
dotfiles_hg_dirty=''
dotfiles_hg_phase=''

dotfiles_hg_var_root() {
	if [ -z "${dotfiles_hg_root}" ] || ! [ -d "${dotfiles_hg_root}/.hg" ] || \
			[[ "${PWD}" != "${dotfiles_hg_root}*" ]]; then
		dotfiles_hg_root="${PWD}"
		while [ -n "${dotfiles_hg_root}" ]; do
			if [ -d "${dotfiles_hg_root}/.hg" ]; then
				return
			fi
			dotfiles_hg_root="${dotfiles_hg_root%/*}"
		done
	fi
	dotfiles_hg_root=''
}

dotfiles_hg_vars() {
	dotfiles_hg_var_root

	if [ -n "${dotfiles_hg_root}" ]; then
		if ! [ -f "${dotfiles_hg_root}/.hg/branch" ]; then
			dotfiles_hg_branch='default'
		else
			dotfiles_hg_branch="$(cat "${dotfiles_hg_root}/.hg/branch" 2>/dev/null)"
		fi
		if [ -f "${dotfiles_hg_root}/.hg/topic" ]; then
			dotfiles_hg_topic="$(cat "${dotfiles_hg_root}/.hg/topic" 2>/dev/null)"
		else
			dotfiles_hg_topic=''
		fi
		if [ -x "$(getpath chg)" ]; then # using hg commands is too slow
			if [ -n "$(chg status 2>/dev/null | head -n1)" ]; then
				dotfiles_hg_dirty='1'
			else
				dotfiles_hg_dirty=''
			fi
			dotfiles_hg_phase="$(chg phase 2>/dev/null | tail -n 1)"
			if [ "${dotfiles_hg_phase%:*}" -eq '-1' ]; then
				dotfiles_hg_phase='draft'
			else
				dotfiles_hg_phase="${dotfiles_hg_phase#*: }"
			fi
		fi
		return
	fi
	dotfiles_hg_branch=''
	dotfiles_hg_topic=''
	dotfiles_hg_dirty=''
	dotfiles_hg_phase=''
}

if [ -n "${BASH_VERSION}" ]; then
	dotfiles_prompt_separator='\[\e[0;31m\]|'
	dotfiles_prompt_git_branch_prefix='\[\e[0;32m\]'
	dotfiles_prompt_git_dirty_prefix='\[\e[33m\]'
	dotfiles_prompt_hg_public_prefix='\[\e[0;32m\]'
	dotfiles_prompt_hg_draft_prefix='\[\e[93m\]'
	dotfiles_prompt_hg_secret_prefix='\[\e[0;31m\]'
	dotfiles_prompt_hg_topic_prefix='\[\e[34m\]'
	dotfiles_prompt_hg_dirty_prefix='\[\e[33m\]'
elif [ -n "${ZSH_VERSION}" ]; then
	dotfiles_prompt_separator="%{$fg[red]%}|"
	dotfiles_prompt_git_branch_prefix="%{$fg[green]%}"
	dotfiles_prompt_git_dirty_prefix="%{$fg[yellow]%}"
	dotfiles_prompt_hg_public_prefix="%{$fg[green]%}"
	dotfiles_prompt_hg_draft_prefix=$'\e[93m'
	dotfiles_prompt_hg_secret_prefix="%{$fg[red]%}"
	dotfiles_prompt_hg_topic_prefix="%{$fg[blue]%}"
	dotfiles_prompt_hg_dirty_prefix="%{$fg[yellow]%}"
else
	dotfiles_prompt_separator='|'
	dotfiles_prompt_git_branch_prefix=''
	dotfiles_prompt_git_dirty_prefix=''
	dotfiles_prompt_hg_public_prefix=''
	dotfiles_prompt_hg_draft_prefix=''
	dotfiles_prompt_hg_secret_prefix=''
	dotfiles_prompt_hg_topic_prefix=''
	dotfiles_prompt_hg_dirty_prefix=''
fi

dotfiles_prompt() {
	dotfiles_git_vars
	dotfiles_hg_vars
	repo_summary=''

	if [ -n "${dotfiles_git_branch}" ]; then
		repo_summary="${dotfiles_prompt_git_branch_prefix}${dotfiles_git_branch}"
		if [ -n "${dotfiles_git_dirty}" ]; then
			repo_summary="${repo_summary}${dotfiles_prompt_git_dirty_prefix}⚡"
		fi
	fi

	if [ -n "${dotfiles_hg_root}" ]; then
		if [ -n "${dotfiles_git_branch}" ]; then
			repo_summary="${repo_summary}${dotfiles_prompt_separator}"
		fi
		if [ -n "${dotfiles_hg_topic}" ]; then
			repo_summary="${repo_summary}${dotfiles_prompt_hg_topic_prefix}${dotfiles_hg_topic}"
		else
			if [ "${dotfiles_hg_phase}" = 'draft' ]; then
				repo_summary="${repo_summary}${dotfiles_prompt_hg_draft_prefix}"
			elif [ "${dotfiles_hg_phase}" = 'public' ]; then
				repo_summary="${repo_summary}${dotfiles_prompt_hg_public_prefix}"
			else # FIXME same color as separator
				repo_summary="${repo_summary}${dotfiles_prompt_hg_secret_prefix}"
			fi
			repo_summary="${repo_summary}${dotfiles_hg_branch}"
		fi
		if [ -n "${dotfiles_hg_dirty}" ]; then
			repo_summary="${repo_summary}${dotfiles_prompt_hg_dirty_prefix}⚡"
		fi
	fi

	if [ -n "${BASH_VERSION}" ]; then
		current_dir="$(dirs +0)"
	elif [ -n "${ZSH_VERSION}" ]; then
		current_dir="$(print -P %0~)"
	else
		if [ "${PWD}" = "${HOME}" ]; then
			current_dir='~'
		else
			current_dir="${PWD/${HOME}'/'/'~/'}"
		fi
	fi

	if [ "${#current_dir}" -gt 15 ]; then # TODO if full prompt over 35? --> OR see ${COLUMNS}
		end='
'
	elif [ -n "${repo_summary}" ]; then
		end=' '
	else
		end=''
	fi

	if [ -n "${BASH_VERSION}" ]; then
		PS1='\[\e[0;35m\]\u\[\e[0;36m\]@\[\e[0;33m\]\h\[\e[0;31m\]:\[\e[0;36m\]\w\[\e[0;31m\]|'"${repo_summary}${end}"'\[\e[0;36m\]⇒\[\e[m\]  '
	elif [ -n "${ZSH_VERSION}" ]; then
		PS1="%{$fg[magenta]%}%n%{$fg[cyan]%}@%{$fg[yellow]%}%m%{$fg[red]%}:%{$fg[cyan]%}%0~%{$fg[red]%}|${repo_summary}${end}%{$fg[cyan]%}⇒%{$reset_color%}  "
	else
		PS1="${USER}@$(hostname -s):${current_dir}|${repo_summary}${end}⇒  "
	fi
}

if [ -n "${BASH_VERSION}" ]; then
	PROMPT_COMMAND='dotfiles_prompt'
elif [ -n "${ZSH_VERSION}" ]; then
	precmd_functions+=(dotfiles_prompt)
fi
