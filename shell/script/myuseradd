# vim:ft=sh
if [ -z "$1" ] || [ "${EUID}" -ne 0 ]; then
	if [ "${EUID}" -ne 0 ]; then
		echo "Needs to be root"
	fi
	echo "Usage: myuseradd username"
	echo
	echo "Optional environment:"
	echo "USERSHELL=/bin/bash"
	echo "BASEHOME=/var/lib/home"
	echo "DOTCONFIG=0 (1 to enable)"
	exit 1
fi

usershell="/bin/bash"
basehome="/var/lib/home"
if [ -n "${BASEHOME}" ]; then
	basehome="${BASEHOME}"
fi
if [ -n "${USERSHELL}" ]; then
	usershell="${USERSHELL}"
fi
if test ! -d "${basehome}"; then
	echo "${basehome} does not exist"
	exit 1
fi

require useradd || exit 1
if [ "${DOTCONFIG}" -eq 1 ]; then
	require sudo || exit 1
fi

if test -d "${basehome}/$1"; then
	echo "User home already exists, skipping."
else
	(set -x; useradd -m -b "${basehome}" -s "${usershell}" "$1")
	ret=$?
	if [ "${ret}" -ne 0 ]; then
		echo "Failed to create user ($ret)."
		exit 2
	fi
fi

if [ "${DOTCONFIG}" -eq 1 ]; then
	sudo -u "$1" test -r "${DOTFILES}/COPYING"
	if [ $? -ne 0 ]; then
		echo "New user cannot read ${DOTFILES}"
		exit 3
	fi
	case "${usershell}" in
		(*zsh) dotshell="zsh" ;;
		(*) dotshell="bash" ;;
	esac
	dotprofile="${basehome}/$1/.profile"
	(set -x; (echo; echo "source '${DOTFILES}/shell/${dotshell}/set-env.${dotshell}'") >> "${dotprofile}")
	ret=$?
	echo ">> ${dotprofile}"
	if [ "${ret}" -ne 0 ]; then
		echo "Couldn't edit .profile"
		exit 4
	fi
fi
