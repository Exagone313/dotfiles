#!/bin/sh
set -e
require stat sha512sum awk || exit 1

if [ -z "$1" ]; then
	echo >&2 'Usage: vthumb <video path> [count:1] [xscale:]'
	exit 1
fi

if ! [ -f "$1" ]; then
	echo >&2 'File does not exist'
	exit 1
fi

file="$1"
count=1
scale=

if [ -n "$2" ]; then
	if ! [ "$2" -gt 0 ]; then
		echo >&2 'Invalid count'
		exit 1
	fi
	count="$2"

	if [ -n "$3" ]; then
		if ! [ "$3" -gt 50 ]; then
			echo >&2 'Invalid xscale'
			exit 1
		fi
		scale=",scale=$3:-1"
	fi
fi

if [ "${count}" -eq 1 ]; then
	filename="$(echo -n "${file##*/}-$(stat --printf=%s "${file}")" | sha512sum | awk '{print $1}').jpg"
	frameargs='-frames:v 1 -update true'
	ffps=120
else
	filename="$(echo -n "${file##*/}-$(stat --printf=%s "${file}")" | sha512sum | awk '{print $1}')-%02d.jpg"
	frameargs="-frames:v ${count}"
	ffps="$((count * 120))"
fi

ffmpeg -y -hide_banner -i "${file}" -ss "${VTHUMB_SS:-3}" -vf select="gt(scene\,0.35),fps=fps=1/${ffps}${scale}" ${frameargs} "${filename}"
