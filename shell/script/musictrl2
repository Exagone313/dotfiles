#!/usr/bin/sh
set -e
require dbus-send grep sed tail cut cat

if [ -z "$1" ]; then
	echo >&2 "Usage: musictrl (list | info | play | pause | toggle | prev[ious] | next)"
	exit 1
fi

list_players() {
	dbus-send --session --dest=org.freedesktop.DBus --type=method_call \
		--print-reply /org/freedesktop/DBus org.freedesktop.DBus.ListNames \
		| grep -Eo 'org\.mpris\.MediaPlayer2\.[.a-zA-Z0-9]+'
}

get_player_status() {
	if [ -n "$1" ]; then
		dbus-send --print-reply --dest="$1" \
			/org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get \
			string:org.mpris.MediaPlayer2.Player string:PlaybackStatus \
			| grep -Eo 'string\s+"[^"]+"' | sed -E 's/string\s+\"([^"]+)\"/\1/'
	fi
}

save_player() {
	if [ -n "$1" ]; then
		echo "$1" > "${TMP}/.musictrl" || true
	fi
}

get_saved_player() {
	cat "${TMP}/.musictrl" 2>/dev/null || true
}

# [--save] player (Play | Pause | PlayPause | Previous | Next)
player_command() {
	if [ "$1" = '--save' ]; then
		shift
		save_player "$1"
	fi
	if [ -n "$1" ] && [ -n "$2" ]; then
		dbus-send --type=method_call --dest="$1" /org/mpris/MediaPlayer2 \
			org.mpris.MediaPlayer2.Player."$2"
	fi
}

get_one_player() {
	players="$(list_players)"
	saved_player="$(get_saved_player)"
	if [ -n "${saved_player}" ]; then
		for player in ${players}; do
			if [ "${saved_player}" = "${player}" ]; then
				echo "${saved_player}"
				return
			fi
		done
		# remove orphaned saved player
		rm -f "${TMP}/.musictrl" || true
	fi
	previous=''
	for current in ${players}; do
		if [ "${previous}" != '' ]; then
			# multiple players, need to select one
			for player in ${players}; do
				# TODO handle player priority
				case "${player}" in
					(*spotify)
						echo "${player}"
						return
						;;
				esac
			done
			# return the first one if no player selected
			echo "${previous}"
			return
		fi
		previous="${current}"
	done
	if [ "${previous}" = '' ]; then
		echo >&2 'No player found'
		exit 1
	fi
	# only one player
	echo "${previous}"
}

case "$1" in
	(list)
		saved_player="$(get_saved_player)"
		for player in $(list_players); do
			suffix=''
			if [ "${player}" = "${saved_player}" ]; then
				suffix=' (Saved)'
			fi
			echo "${player} ($(get_player_status "${player}"))${suffix}"
		done
		;;
	(info)
		player="$(get_one_player)"
		if [ "$(get_player_status "${player}")" = 'Playing' ]; then
			metadata="$(dbus-send --print-reply --dest="${player}" /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:org.mpris.MediaPlayer2.Player string:Metadata)"
			artist="$(echo "${metadata}" | grep -A 2 -E 'string\s+"xesam:artist"' | tail -n 1 | cut -d '"' -f 2)"
			title="$(echo "${metadata}" | grep -A 1 -E 'string\s+"xesam:title"' | grep -vE 'string\s+"xesam:title"' | cut -d '"' -f 2)"
			echo "${artist} - ${title}"
		fi
		;;
	(toggle)
		player_command --save "$(get_one_player)" PlayPause
		;;
	(play)
		player_command --save "$(get_one_player)" Play
		;;
	(pause)
		player_command --save "$(get_one_player)" Pause
		;;
	(prev*)
		player_command --save "$(get_one_player)" Previous
		;;
	(next)
		player_command --save "$(get_one_player)" Next
		;;
	(pauseall)
		for player in $(list_players); do
			player_command "${player}" Pause
		done
		;;
	(save)
		if [ -z "$2" ]; then
			save_player "$(get_one_player)"
			exit
		fi
		for player in $(list_players); do
			case "${player}" in
				(*"$2"*)
					save_player "${player}"
					exit
					;;
			esac
		done
		echo >&2 'No matching player found'
		exit 1
		;;
	(*)
		echo >&2 'Unknown command'
		exit 1
		;;
esac
