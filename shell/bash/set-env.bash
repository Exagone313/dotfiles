case $- in *i*) interactive_shell=1; esac

for cmd in "getpath" "realpath"; do
	source "${BASH_SOURCE[0]%/*}/command/${cmd}"
done
export DOTFILES="$(realpath "${BASH_SOURCE[0]%/*}/../..")"
export DOTSHELL=bash
export DOTSHELLCMD="${BASH}"
source "${BASH_SOURCE[0]%/*}/autoload.bash"
source "${DOTFILES}/shell/autoload.sh"
