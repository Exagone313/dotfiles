if [[ -o interactive ]]; then
	interactive_shell=1
fi

source "${0:a:h}/command/getpath"

dotfiles_path="${0:a:h}/../.."
export DOTFILES="${dotfiles_path:A}"
unset dotfiles_path
export DOTSHELL=zsh
if [ -z "${ZSH_ARGZERO}" ]; then # thanks okdana on Freenode
	() {
		setopt local_options posix_argzero
		ZSH_ARGZERO="${commands[$0]}"
		if [ -z "${ZSH_ARGZERO}" ]; then # shell command not found?!
			ZSH_ARGZERO="$0"
		fi
	}
fi
export DOTSHELLCMD="${ZSH_ARGZERO}"
case "${DOTSHELLCMD}" in
	(-*) DOTSHELLCMD="${DOTSHELLCMD:1}" ;;
	(*) ;;
esac
source "${0:a:h}/autoload.zsh"
source "${DOTFILES}/shell/autoload.sh"
