if [ -z "${HISTFILE}" ]; then
	if [ -z "${ZDOTDIR}" ]; then
		HISTFILE=~/.zsh_history
	else
		HISTFILE="${ZDOTDIR}/.zsh_history"
	fi
fi

SAVEHIST=200000
HISTSIZE=200000

setopt EXTENDED_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_NO_STORE
setopt HIST_REDUCE_BLANKS
setopt HIST_VERIFY
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY

autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
