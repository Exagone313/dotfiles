if [ -n "${interactive_shell}" ]; then
	autoload colors && colors
	source "${0:a:h}/completion.zsh"
	source "${0:a:h}/input.zsh"
	source "${0:a:h}/colors.zsh"
	source "${0:a:h}/history.zsh"
	source "${0:a:h}/misc.zsh"
	source "${0:a:h}/title.zsh"
fi
