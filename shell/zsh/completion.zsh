autoload -Uz compinit && compinit

setopt ALWAYS_TO_END
setopt AUTO_MENU
setopt COMPLETE_IN_WORD
setopt GLOB_COMPLETE
setopt LIST_PACKED

# enable menu completion
zstyle ':completion:*' menu select

# disable case insensitive completion (copied from OMZ)
zstyle ':completion:*' matcher-list 'r:|=*' 'l:|=* r:|=*'
