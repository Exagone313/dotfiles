if [ -x "$(getpath dircolors)" ]; then
	eval "$(dircolors)"
fi
if ls --color=auto / >/dev/null 2>&1; then
	alias ls='ls --color=auto'
fi
# copied from OMZ
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
