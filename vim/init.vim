" directories

set backupdir=~/.cache/vim/backup//
set directory=~/.cache/vim/swap//
set undodir=~/.cache/vim/undo//
if !isdirectory(&backupdir)
  call mkdir(&backupdir, "p", 0700)
endif
if !isdirectory(&directory)
  call mkdir(&directory, "p", 0700)
endif
if !isdirectory(&undodir)
  call mkdir(&undodir, "p", 0700)
endif

" basic options

set nu
set cursorline
set mouse=nc
set wildignore=*.a,*.d,*.o
set foldlevelstart=99
set noautoread
set title

" status line
set statusline=%<%f\ %y%m%r%=%-12.(%l:%c%)\ %-3.($%B%)\ %-5L\ %P

" theme

let g:jellybeans_overrides = {
\  'background': { 'ctermbg': 'none', '256ctermbg': 'none' },
\}
colorscheme jellybeans

" binds

nnoremap q a
nnoremap Q A
" move between tabs with tab or shift+tab
nnoremap <Tab> gt
nnoremap <S-Tab> gT

" leader binds

let mapleader = ","
nnoremap <leader>t :tab split<CR>
" hide search
nnoremap <leader><space> :nohlsearch<CR>
" system clipboard
if has("clipboard")
  nnoremap <leader>p "+p
  vnoremap <leader>p "+p
  nnoremap <leader>P "+P
  vnoremap <leader>P "+P
  vnoremap <leader>y "+y
  nnoremap <leader>Y "+Y
endif
" counts (echo v:statusmsg)
noremap <leader>n g<C-g>
" open current directory
nnoremap <leader>e :e %:h<CR>

" plugin settings

let g:indent_guides_enable_on_vim_startup = 1
" ale
let g:ale_linters_explicit = 1
let g:ale_linters = {
\  'bash': ['shellcheck'],
\  'sh': ['shellcheck'],
\  'zsh': ['shellcheck'],
\  'python': ['flake8', 'pylint'],
\}
let g:ale_disable_lsp = 1
let g:ale_use_neovim_diagnostics_api = 1
let g:ale_echo_cursor = 0
let g:ale_hover_cursor = 0

" file types

filetype plugin indent on " must be run early

set fo-=c fo-=r noet ts=4 sts=0 sw=0

function! DefaultFileType()
  setl fo-=c fo-=r sts=0 sw=0
  hi ColorColumn cterm=reverse
endfunction

augroup filetypesconfig
  autocmd!

  autocmd BufNewFile,BufReadPre,FileReadPre * call DefaultFileType()
  autocmd FileType * call DefaultFileType()

  autocmd BufNewFile,BufReadPost *.sch setl ft=xml
  autocmd BufNewFile,BufReadPost *.vue setl ft=html
  autocmd BufNewFile,BufReadPost *.jad setl ft=java
  autocmd BufNewFile,BufReadPost *.jsm setl ft=javascript
  autocmd BufNewFile,BufReadPost composer.json setl et ts=4

  autocmd FileType PKGBUILD,css,dtd,elixir,html,javascript,javascript.jsx,javascriptreact,json,prisma,scss,sls,typescript,typescript.tsx,typescriptreact,vim,xml,xsd,xslt,yaml setl et ts=2
  autocmd FileType php,python,zig setl et ts=4
  autocmd FileType c,cpp,make,text setl noet ts=4
  autocmd FileType c,cpp setl tw=80 cc=+1
augroup END

" optional packages

silent! packadd ale

" neovim features

if has("nvim")
  " exit terminal mode
  tnoremap <Esc> <C-\><C-n>
  " change ShaDa path
  set shada+=n~/.cache/vim/shada
  " fix line yanking in Neovim 0.6+
  silent! unmap Y
  " load nvim-lspconfig
  silent! packadd nvim-lspconfig
  if exists("g:lspconfig")
    lua require("lsp_config")
  endif
endif
