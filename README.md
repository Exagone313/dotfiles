# Personal configuration files

## About

The goal of this repository is to have these files around for ease.

All code made by another author will either have its repository forked or it will be included here. Note that I want to review all these codes.

## Licensing

Unless otherwise stated:
* software in this repository is released under the 3-Clause BSD License (see `COPYING`)
* configuration files in `/config` are released into the public domain (see `config/COPYING`).
